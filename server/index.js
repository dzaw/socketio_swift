//require necessary modules
var http = require('http')
  , express = require('express')
  , socketIO = require("socket.io")
  , path = require('path');

//initialize our application
var app = express();
app.use(express.static(path.join(__dirname, 'assets')));
var server = http.createServer(app).listen(42474);
var io = socketIO.listen(server);

console.log('starting server');

//settings
var settings = {
  'view_directory': '/views'
}


app.get('/', function(request, response){
  response.sendfile(__dirname + settings.view_directory + '/index.html')
});


//chat using socket.io
io.sockets.on('connection', function(client){
  //when client sends a join event
  client.on('join', function(data){
    client.username = data;
    console.log('client joined: $'+client.username);
    client.emit("ping");
    client.broadcast.emit('message', { message: data + " connected!", nickname: "server" });
  });

  //when client sends a message
  client.on('message', function(data){
    console.log("message from $"+client.username + " : " + data);
    client.broadcast.emit('message', { message: data, nickname: client.username });   
  });

})
